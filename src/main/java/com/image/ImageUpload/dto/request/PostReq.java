package com.image.ImageUpload.dto.request;

import lombok.Data;

import java.util.Date;

@Data
public class PostReq {

    String name;

    String description;

}
