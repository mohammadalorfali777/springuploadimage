package com.image.ImageUpload.controllers;

import com.image.ImageUpload.ImageService.ImageStorage;
import com.image.ImageUpload.Repositories.PostRepository;
import com.image.ImageUpload.dto.request.PostReq;
import com.image.ImageUpload.dto.response.Message;
import com.image.ImageUpload.models.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.Optional;

import static org.springframework.http.ResponseEntity.status;

@RestController
@RequestMapping("post")
public class PostController {
    @Autowired
    ImageStorage imageStorage;

    @Autowired
    PostRepository postRepository;

    @PostMapping("/")
    public ResponseEntity<Message> upload(@RequestParam("image")MultipartFile image, PostReq post){
        try {
            String imageName = imageStorage.save(image);
            Post post1 = new Post(post.getName(), post.getDescription(),imageName,new Date());
            postRepository.save(post1);
            return ResponseEntity.status(HttpStatus.OK).body(new Message("Message has been uploaded: "+image.getOriginalFilename()));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new Message("Could not upload the file: "+image.getOriginalFilename()));
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Optional<Post>> getFile(@PathVariable Long id) {
        Optional<Post> post =  postRepository.findById(id);
        post.get().setImage("/uploads/"+post.get().getImage());
        return ResponseEntity.ok().body(post);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Message> delete(@PathVariable Long id) throws IOException {
        Post post = postRepository.getReferenceById(id);
        imageStorage.delete(post.getImage());
        postRepository.delete(post);

        return ResponseEntity.status(HttpStatus.OK).body(new Message("Post has been deleted"));
    }
}
