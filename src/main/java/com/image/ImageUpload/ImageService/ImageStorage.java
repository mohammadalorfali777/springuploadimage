package com.image.ImageUpload.ImageService;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Path;

public interface ImageStorage {

    public void init();

    public String save(MultipartFile file);

    public void delete(String filename) throws IOException;
}
