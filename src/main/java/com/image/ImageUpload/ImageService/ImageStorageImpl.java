package com.image.ImageUpload.ImageService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.crypto.Data;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

@Service
public class ImageStorageImpl implements ImageStorage{

    private final Path directory = Paths.get("src/main/resources/static/uploads");

    @Override
    public void init() {
        new File("src/main/resources/static/uploads").mkdir();
    }

    @Override
    public String save(MultipartFile file) {
        try {
            String newName = new Date()+file.getOriginalFilename();
            Files.copy(file.getInputStream(), this.directory.resolve(newName));
            return newName;
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
    }

    @Override
    public void delete(String filename) throws IOException {
        FileSystemUtils.deleteRecursively(directory.resolve(filename));
    }
}
