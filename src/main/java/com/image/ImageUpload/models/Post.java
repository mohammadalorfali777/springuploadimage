package com.image.ImageUpload.models;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Date;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    @NotNull
    @Column(unique = true)
    @Size(max = 199)
    private String name;

    @Column(columnDefinition = "MEDIUMTEXT")
    private String description;

    private String image;

    private Date date;

    @Version
    private int version;

    public Post(String name, String description, String image, Date date){
        this.name = name;
        this.description = description;
        this.image = image;
        this.date = date;
    }
}
