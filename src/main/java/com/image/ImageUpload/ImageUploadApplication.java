package com.image.ImageUpload;

import com.image.ImageUpload.ImageService.ImageStorage;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Resource;

@SpringBootApplication
public class ImageUploadApplication implements CommandLineRunner {

	@Resource
	ImageStorage imageStorage;
	public static void main(String[] args) {
		SpringApplication.run(ImageUploadApplication.class, args);
	}

	@Override
	public void run(String... arg) throws Exception {
		imageStorage.init();
	}

}
